FROM python:3.9.1
ADD . /autocert-api
WORKDIR /autocert-api
RUN chmod 777 gunicorn_starter.sh
RUN pip install -r requirements.txt
EXPOSE 6000:6000
ENTRYPOINT ["./gunicorn_starter.sh"]