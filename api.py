from flask import Flask, request, jsonify
from functions.certificate import CreateCert
from functions.send_email import SendMail
import os


app = Flask(__name__)
api_token = os.environ.get('AUTOCERT_API_TOKEN')

@app.route('/create', methods=["POST"])
def create():
    
    if request.json['code'] == api_token:

        name = request.json['name']
        course = request.json['course']
        email = request.json['email']
        
        new_object = {
            'name': name,
            'course': course,
            'email': email,
            'status': 'success'
        }
        CreateCert(name=name, course=course, email=email).create_pdf()

        SendMail(receiver=str(email), std_name=str(
            name), course=str(course)).send()

        return jsonify(new_object), 200
    else:
        return 'Request not allowed', 403


if __name__ == '__main__':
    app.run(debug=False, port=6000)
