#!/bin/sh
mkdir certificates
gunicorn -w 4 -b 0.0.0.0:6000 --access-logfile gunicorn-log.log api:app