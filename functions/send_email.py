import smtplib
from email import encoders
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from pathlib import Path
import os


# enviando email
class SendMail:

    def __init__(self, receiver=str, std_name=str, course=str):
        email_token=str(os.environ['EMAIL_SECRET_KEY'])
        self.course=course
        self.std_name = std_name
        self.anexo = Path(f"./certificates/Autocert-Certificado_de_participacao-{self.std_name}.pdf")
        self.server = smtplib.SMTP('smtp.gmail.com', 587)
        self.email = str(os.environ['AUTOCERT_EMAIL_FROM'])
        self.key = email_token
        self.msg = MIMEMultipart()
        self.email_destiny = receiver
        self.template = ''
        with open('functions/html_template.html','r') as template:
            self.template = template.read()
        self.html = str(self.template.replace('{nome}', str(self.std_name)).replace('{course}', str(self.course)))
        
    def send(self):
        self.server.ehlo()
        self.server.starttls()
        self.server.ehlo()
        self.server.login(self.email, self.key)

        # Mensage configuration
        self.msg["From"] = str(os.environ['AUTOCERT_EMAIL_FROM'])
        self.msg["to"] = self.email_destiny
        self.msg["Subject"] = str(os.environ['AUTOCERT_EMAIL_SUBJECT'])
        self.msg["Bcc"] = self.email_destiny

        # Send email
        self.msg.attach(MIMEText(self.html, 'html'))
        filename = self.anexo # Direotry of file
        with open(filename, 'rb') as attachment:
            part = MIMEBase('application', 'octet-stream')
            part.set_payload(attachment.read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', f'attachment; filename={filename}')
        self.msg.attach(part)

        # Drop the server
        self.server.send_message(self.msg)
        self.server.quit()
