from weasyprint import HTML, CSS
from weasyprint.fonts import FontConfiguration
import random
hash = random.getrandbits(128)

class CreateCert:
    def __init__(self, name=str, course=str, email=str):
        self.name = name
        self.course = course
        self.email = email
        with open('functions/html_template.html', 'r+') as template:
            html = template.read()
            self.replacer = html.replace('{nome}', self.name).replace('{course}', self.course)
        self.font_config = FontConfiguration()

    def create_pdf(self):
        template = HTML(string=self.replacer)
        template.write_pdf(f'certificates/Autocert-Certificado_de_participacao-{self.name}.pdf', font_config=self.font_config)

    def create_png(self):
        template = HTML(string=self.replacer)
        template.write_png(f'certificates/Autocert-Certificado_de_participacao-{self.name}.png', font_config=self.font_config)
